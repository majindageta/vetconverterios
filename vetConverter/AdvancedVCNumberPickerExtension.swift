//
//  AdvancedVCNumberPickerExtension.swift
//  vetConverter
//
//  Created by Davide Ricci on 02/10/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import Foundation
import UIKit

extension AdvancedViewController {
    
    func initializeNPicker() {
        numericPicker.backgroundColor = lighterBackground
        numericPicker.tintColor = white
        numericPicker.addTarget(self, action: #selector(codeValueChanged), for: .valueChanged)
    }
    
    @objc func codeValueChanged() {
        if (selectedButton == Converter.PICKER_SELECTED.principle) {
            activePrinciple.text = Converter.formatDecimal(value: numericPicker.value)
        }
        if (selectedButton == Converter.PICKER_SELECTED.total) {
            total.text = Converter.formatDecimal(value: numericPicker.value)
        }
        if (selectedButton == Converter.PICKER_SELECTED.target) {
            targetPrinciple.text = Converter.formatDecimal(value: numericPicker.value)
        }
    }
    
    func resetNumericPickerValueAccordingToText(textField: UITextField) {
        if (textField.text != nil) {
            let doubleValue : Double? = Double(textField.text!)
            if (doubleValue != nil) {
                numericPicker.value = doubleValue!
            } else {
                numericPicker.value = 0
            }
        } else {
            numericPicker.value = 0
        }
    }
}



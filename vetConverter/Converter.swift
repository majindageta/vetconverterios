//
//  Converter.swift
//  vetConverter
//
//  Created by Davide Ricci on 09/08/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import UIKit

class Converter: NSObject {
    
    public enum MISURING_UNIT : String {
        case ml = "ml"
        case cl = "cl"
        case dl = "dl"
        case mg = "mg"
        case cg = "cg"
        case dg = "dg"
        case l = "l"
        case g = "g"
        case percentage = "%"
    }
    
    public enum PICKER_SELECTED : Int {
        case principle = 1
        case total = 2
        case target = 3
    }
    
    static public let misuringUnitSolution = [MISURING_UNIT.ml.rawValue,  MISURING_UNIT.cl.rawValue,  MISURING_UNIT.dl.rawValue, MISURING_UNIT.l.rawValue, MISURING_UNIT.mg.rawValue, MISURING_UNIT.cg.rawValue, MISURING_UNIT.dg.rawValue, MISURING_UNIT.g.rawValue, "%"]
    static public let misuringUnitTotal = [MISURING_UNIT.ml.rawValue,  MISURING_UNIT.cl.rawValue,  MISURING_UNIT.dl.rawValue, MISURING_UNIT.l.rawValue, MISURING_UNIT.mg.rawValue, MISURING_UNIT.cg.rawValue, MISURING_UNIT.dg.rawValue, MISURING_UNIT.g.rawValue]
    
    static var actualMisuringUnitSolution = MISURING_UNIT.mg    //1
    static var actualMisuringUnitTotal = MISURING_UNIT.ml       //2
    static var actualMisuringUnitTarget = MISURING_UNIT.mg      //3
    static var actualMisuringUnitResult = MISURING_UNIT.ml      //4
    
    public static func makeConvertion (activePrinciples: Double, total: Double, target: Double, weight: Double) -> Double{
        
        if (actualMisuringUnitSolution == .percentage) {
            //percentage calculation: activePrinciples is a percentage from 1 to 100
            //need to calculate first the value of the actual percentage then use it to do proportion for target
            
            //New fantastic calculation (considera sempre mg/ml oppure g/L
            // % è principio attivo / 100 g o ml quindi 9% soluzione ho 9 grammi in 100ml
            var actualTarget : Double = target
            if (actualMisuringUnitTarget == .mg || actualMisuringUnitTarget == .ml) {
                actualTarget = target / 1000
            }
            if (actualMisuringUnitTarget == .cg || actualMisuringUnitTarget == .cl) {
                actualTarget = target / 100
            }
            if (actualMisuringUnitTarget == .dg || actualMisuringUnitTarget == .dl) {
                actualTarget = target / 10
            }
            
            let fantasticCalculation : Double = actualTarget / activePrinciples * 100
            return weight != 0 ? fantasticCalculation * weight : fantasticCalculation;
        } else {
            return weight != 0 ? (target/activePrinciples) * (total) * weight : (target/activePrinciples) * (total)
        }
    }
    
    static func returnMultiplier(unit: Double?, item: Int) -> Double {
        var value : Double? = unit
        if (value != nil) {
            if (actualMisuringUnitResult == .ml || actualMisuringUnitResult == .mg) {
                if (misuringUnitSolution[item] == MISURING_UNIT.cl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.cg.rawValue) {
                    value = value! / 10
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.dl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.dg.rawValue) {
                    value = value! / 100
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.l.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.g.rawValue) {
                    value = value! / 1000
                }
            } else if (actualMisuringUnitResult == .cl || actualMisuringUnitResult == .cg) {
                if (misuringUnitSolution[item] == MISURING_UNIT.ml.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.mg.rawValue) {
                    value = value! * 10
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.dl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.dg.rawValue) {
                    value = value! / 10
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.l.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.g.rawValue) {
                    value = value! / 100
                }
            } else if (actualMisuringUnitResult == .dl || actualMisuringUnitResult == .dg) {
                
                if (misuringUnitSolution[item] == MISURING_UNIT.ml.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.mg.rawValue) {
                    value = value! * 100
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.cl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.cg.rawValue) {
                    value = value! * 10
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.l.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.g.rawValue) {
                    value = value! / 10
                }
            } else if (actualMisuringUnitResult == .l || actualMisuringUnitResult == .g) {
                if (misuringUnitSolution[item] == MISURING_UNIT.ml.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.mg.rawValue) {
                    value = value! * 1000
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.cl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.cg.rawValue) {
                    value = value! * 100
                }
                if (misuringUnitSolution[item] == MISURING_UNIT.dl.rawValue ||
                    misuringUnitSolution[item] == MISURING_UNIT.dg.rawValue) {
                    value = value! * 10
                }
            }
        }
        return value!
    }
    
    static func formatDecimal(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 5
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber)
        return result!
    }
    
    static func revertFormatDecimalToDouble(value: String) -> Double {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 5
        formatter.locale = Locale(identifier: Locale.current.identifier)
        var result : NSNumber?
        if (Locale.current.identifier.lowercased().contains("it")) {
            result = formatter.number(from: value.replacingOccurrences(of: ".", with: ""))
        } else {
            result = formatter.number(from: value.replacingOccurrences(of: ",", with: ""))
        }
        if (result != nil) {
            return Double((result?.doubleValue)!)
        } else {
            return 0
        }
    }
    
    static func selectUnitSolution (value: String, actualUnitIndex: Int) {
        
        for s in misuringUnitSolution {
            if s == value {
                switch actualUnitIndex {
                case 1:
                    actualMisuringUnitSolution = MISURING_UNIT.init(rawValue: value)!
                    break
                case 2:
                    actualMisuringUnitTotal = MISURING_UNIT.init(rawValue: value)!
                    break
                case 3:
                    actualMisuringUnitTarget = MISURING_UNIT.init(rawValue: value)!
                    break
                case 4:
                    actualMisuringUnitResult = MISURING_UNIT.init(rawValue: value)!
                    break
                default:
                    break
                }
            }
        }
    }
}

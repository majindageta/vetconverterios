//
//  ViewController.swift
//  vetConverter
//
//  Created by Davide Ricci on 05/08/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        //TODO: when the basic view is finished, we can go to advanced or basic, accordingly with the sharedPref
        self.performSegue(withIdentifier: "advancedSegue", sender: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}


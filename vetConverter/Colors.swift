//
//  Colors.swift
//  vetConverter
//
//  Created by Davide Ricci on 02/10/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import UIKit

let background = UIColor.init(red: 49/255, green: 54/255, blue: 68/255, alpha: 1.0)
let lighterBackground = UIColor.init(red: 62/255, green: 68/255, blue: 83/255, alpha: 1.0)
let contrast = UIColor.init(red: 28/255, green: 94/255, blue: 113/255, alpha: 1.0)
let bluecontrast = UIColor.init(red: 38/255, green: 50/255, blue: 79/255, alpha: 1.0)
let white = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)


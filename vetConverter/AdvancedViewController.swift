//
//  AdvancedViewController.swift
//  vetConverter
//
//  Created by Davide Ricci on 05/08/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import UIKit
import NumericPicker

class AdvancedViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var activePrincipleLabel: UILabel!
    @IBOutlet weak var activePrinciple: UITextField!
    @IBOutlet weak var misuringUnitAKPicker: AKPickerView!
    
    @IBOutlet weak var total: UITextField!
    @IBOutlet weak var totalAKPicker: AKPickerView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalNPButton: UIButton!
    
    @IBOutlet weak var targetPrinciple: UITextField!
    @IBOutlet weak var targetAKPicker: AKPickerView!
    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var resultAKPicker: AKPickerView!
    
    @IBOutlet weak var resultHelp: UILabel!
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var calculate: UIButton!
    
    @IBOutlet weak var numericPicker: NumericPicker!
    
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weight: UITextField!
    
    @IBOutlet weak var changeVCType: UIButton!
    
    @IBOutlet weak var resultBorderView: UIView!
    @IBOutlet weak var targetBorderView: UIView!
    @IBOutlet weak var principleBorderView: UIView!
    @IBOutlet weak var totalBorderView: UIView!
    
    var selectedButton : Converter.PICKER_SELECTED = Converter.PICKER_SELECTED.principle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = background
        
        initializeAKPickers()
        initializeNPicker()
    
        setUpUIInterface()
        setUpLocalizedStrings()
        
        //TODO: when basic view is implemented, show the button and do stuff
        changeVCType.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showDisclaimerAlert()
    }
    
    func showDisclaimerAlert() {
        let userDefaults = UserDefaults()
        if !userDefaults.bool(forKey: "disclaimer") {
            userDefaults.set(true, forKey: "disclaimer")
            
            let alert = UIAlertController(title: localizedString(key: "alert.disclaimer.title"), message: localizedString(key: "alert.disclaimer.message"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }

    @IBAction func calculateAction(_ sender: Any) {
        hideKeyboard()
        resultAKPicker.selectItem(totalAKPicker.selectedItem)
        let activePrinciplesDouble : Double? = Double(activePrinciple.text!)
        let totalDouble : Double? = Double(total.text!)
        let targetDouble : Double? = Double(targetPrinciple.text!)
        let weightDouble : Double? = Double(weight.text!)
        
        if (activePrinciplesDouble != nil && totalDouble != nil && targetDouble != nil) {
            resultAKPicker.isHidden = false
            resultBorderView.isHidden = false
            result.isHidden = false
            
            resultHelp.text = setUpLocalizedStringForResult(weightDouble: weightDouble)
            
            result.text = Converter.formatDecimal(value: Converter.makeConvertion(activePrinciples: activePrinciplesDouble!, total: totalDouble!, target: targetDouble!, weight: weightDouble != nil ?  weightDouble! : 0))
        } else {
            resultHelp.text = localizedString(key: "result.help.error.text")
        }
    }
    
    @IBAction func NPButtonPressed(_ sender: Any) {
        if (numericPicker.isHidden) {
            hideKeyboard()
            let button = sender as! UIButton
            switch button.tag {
                case 1:
                    selectedButton = Converter.PICKER_SELECTED.principle
                    resetNumericPickerValueAccordingToText(textField: activePrinciple)
                break
                case 2:
                    selectedButton = Converter.PICKER_SELECTED.total
                    resetNumericPickerValueAccordingToText(textField: total)
                break
                case 3:
                    selectedButton = Converter.PICKER_SELECTED.target
                    resetNumericPickerValueAccordingToText(textField: targetPrinciple)
                break
                default:
                break
            }
            numericPicker.isHidden = false
        } else {
             numericPicker.isHidden = true
        }
    }
    
    @IBAction func goToDefaultButtonPressed(_ sender: Any) {
        //TODO:
//        set variable nsuserdefaults
//        launch other view
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        numericPicker.isHidden = true
    }
    
    func hideKeyboard() {
        numericPicker.isHidden = true
        self.view.endEditing(true)
    }
}

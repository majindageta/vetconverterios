//
//  AdvancedVCAKPickerExtension.swift
//  vetConverter
//
//  Created by Davide Ricci on 02/10/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import Foundation
import UIKit

extension AdvancedViewController : AKPickerViewDataSource, AKPickerViewDelegate {
    
    func initializeAKPickers() {
        setPropertiesFor(aKPicker: misuringUnitAKPicker)
        setPropertiesFor(aKPicker: totalAKPicker)
        setPropertiesFor(aKPicker: resultAKPicker)
        setPropertiesFor(aKPicker: targetAKPicker)
        
        misuringUnitAKPicker.selectItem(4)
        totalAKPicker.selectItem(0)
        resultAKPicker.selectItem(4)
        targetAKPicker.selectItem(4)
    }
    
    func setPropertiesFor(aKPicker: AKPickerView) {
        aKPicker.delegate = self
        aKPicker.dataSource = self
        aKPicker.font = UIFont(name: "HelveticaNeue-Light", size: 22)!
        aKPicker.highlightedFont = UIFont(name: "HelveticaNeue", size: 22)!
        aKPicker.pickerViewStyle = .flat
        aKPicker.textColor = contrast
        aKPicker.highlightedTextColor = white
        aKPicker.maskDisabled = false
        aKPicker.backgroundColor = lighterBackground
        aKPicker.reloadData()
    }
    
    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        hideKeyboard()
        if pickerView == self.misuringUnitAKPicker {
            Converter.selectUnitSolution(value: Converter.misuringUnitSolution[item], actualUnitIndex: 1)
            if (Converter.misuringUnitSolution[item] == "%") {
                targetAKPicker.isHidden = false
                targetBorderView.isHidden = false
                targetAKPicker.selectItem(7)
                resultAKPicker.selectItem(7)
                
                total.text = "100";
                total.isUserInteractionEnabled = false
                total.isEnabled = false
                totalAKPicker.isUserInteractionEnabled = false
                totalNPButton.isEnabled = false
            } else {
                targetAKPicker.isHidden = true
                targetBorderView.isHidden = true
                targetAKPicker.selectItem(4)
                resultAKPicker.selectItem(4)
                
                total.isUserInteractionEnabled = true
                total.isEnabled = true
                totalAKPicker.isUserInteractionEnabled = true
                totalNPButton.isEnabled = true
            }
        }
        
        if pickerView == self.totalAKPicker {
            Converter.selectUnitSolution(value: Converter.misuringUnitSolution[item], actualUnitIndex: 2)
            if (Converter.misuringUnitSolution[item] != "%") {
                Converter.selectUnitSolution(value: Converter.misuringUnitSolution[item], actualUnitIndex: 4)
                resultAKPicker.selectItem(item)
            }
        }
        
        if pickerView == self.targetAKPicker {
            Converter.selectUnitSolution(value: Converter.misuringUnitTotal[item], actualUnitIndex: 3)
        }
        
        if pickerView == self.resultAKPicker {
            var value : Double? = Converter.revertFormatDecimalToDouble(value: result.text!)
            if (value != nil) {
                value = Converter.returnMultiplier(unit: value, item: item)
                result.text = Converter.formatDecimal(value: value!)
                var resultHelpString : String! = resultHelp.text!
                if (resultHelpString != nil && resultHelpString.count > 4) {
                    if let range = resultHelpString.range(of: Converter.actualMisuringUnitResult.rawValue + ":") {
                        let substring = resultHelpString[..<range.lowerBound]
                        resultHelpString = substring + Converter.misuringUnitSolution[item] + ":"
                        resultHelp.text = resultHelpString
                    }
                }
            }
            Converter.selectUnitSolution(value: Converter.misuringUnitSolution[item], actualUnitIndex: 4)
        }
    }
    
    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        if pickerView == self.misuringUnitAKPicker {
            return Converter.misuringUnitSolution.count
        }
        return Converter.misuringUnitTotal.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
        if pickerView == self.misuringUnitAKPicker {
            return Converter.misuringUnitSolution[item]
        }
        return Converter.misuringUnitTotal[item]
    }
    
    func pickerView(_ pickerView: AKPickerView, marginForItem item: Int) -> CGSize {
        return CGSize.init(width: 15, height: 5)
    }
}

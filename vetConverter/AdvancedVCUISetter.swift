//
//  AdvancedVCUISetter.swift
//  vetConverter
//
//  Created by Davide Ricci on 22/12/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import UIKit

extension AdvancedViewController {
    
    func setUpUIInterface() {
        
        activePrinciple.textColor = white
        activePrinciple.delegate = self
        activePrinciple.backgroundColor = lighterBackground
        activePrinciple.attributedPlaceholder = NSAttributedString.init(string: "-", attributes: [NSAttributedStringKey.foregroundColor:white])
        
        total.textColor = white
        total.delegate = self
        total.backgroundColor = lighterBackground
        total.attributedPlaceholder = NSAttributedString.init(string: "-", attributes:
            [NSAttributedStringKey.foregroundColor:white])
        
        targetPrinciple.textColor = white
        targetPrinciple.delegate = self
        targetPrinciple.backgroundColor = lighterBackground
        targetPrinciple.attributedPlaceholder = NSAttributedString.init(string: "-", attributes: [NSAttributedStringKey.foregroundColor:white])
        
        weight.textColor = white
        weight.delegate = self
        weight.backgroundColor = lighterBackground
        weight.attributedPlaceholder = NSAttributedString.init(string: "-", attributes:
            [NSAttributedStringKey.foregroundColor:white])
        
        total.textColor = white
        result.textColor = white
        targetPrinciple.textColor = white
        numericPicker.isHidden = true
        resultAKPicker.isHidden = true
        resultBorderView.isHidden = true
        result.isHidden = true
        
        calculate.layer.cornerRadius = 5
        
        setBorderColor(view: principleBorderView)
        setBorderColor(view: targetBorderView)
        setBorderColor(view: totalBorderView)
        setBorderColor(view: resultBorderView)
    }
    
    func setUpLocalizedStrings() {
        activePrincipleLabel.text = localizedString(key: "active.principle.text")
        totalLabel.text = localizedString(key: "total.text")
        targetLabel.text = localizedString(key: "target.text")
        weightLabel.text = localizedString(key: "weight.text")
        
        calculate.setTitle(localizedString(key: "calculate.text"), for: UIControlState.normal)
    }
    
    func setUpLocalizedStringForResult(weightDouble : Double!) -> String{
        var string : String = ""
        //TODO: il peso ha troppi zeri perchè lo prende tipo float
        if (Converter.misuringUnitSolution[misuringUnitAKPicker.selectedItem] == "%") {
            
            if (weightDouble != nil) {
                string = String(format: localizedString(key: "result.help.percentage.weight.text"),activePrinciple.text!, total.text!, Converter.misuringUnitSolution[totalAKPicker.selectedItem], targetPrinciple.text!, Converter.misuringUnitSolution[targetAKPicker.selectedItem], Converter.formatDecimal(value: weightDouble), Converter.misuringUnitSolution[resultAKPicker.selectedItem]);
            } else {
                string = String.localizedStringWithFormat(localizedString(key: "result.help.percentage.text"), activePrinciple.text!, total.text!, Converter.misuringUnitSolution[totalAKPicker.selectedItem], targetPrinciple.text!, Converter.misuringUnitSolution[targetAKPicker.selectedItem], Converter.misuringUnitSolution[resultAKPicker.selectedItem]);
            }
        } else {
            if (weightDouble != nil) {
                string = String(format: localizedString(key: "result.help.weight.text"),activePrinciple.text!, Converter.misuringUnitSolution[misuringUnitAKPicker.selectedItem], total.text!, Converter.misuringUnitSolution[totalAKPicker.selectedItem], targetPrinciple.text!, Converter.misuringUnitSolution[targetAKPicker.selectedItem], Converter.formatDecimal(value: weightDouble), Converter.misuringUnitSolution[resultAKPicker.selectedItem]);
            } else {
                string = String.localizedStringWithFormat(localizedString(key: "result.help.text"), activePrinciple.text!, Converter.misuringUnitSolution[misuringUnitAKPicker.selectedItem], total.text!, Converter.misuringUnitSolution[totalAKPicker.selectedItem], targetPrinciple.text!, Converter.misuringUnitSolution[targetAKPicker.selectedItem], Converter.misuringUnitSolution[resultAKPicker.selectedItem]);
            }
        }
        return string
    }
    
    func setBorderColor(view: UIView!) {
        view.backgroundColor = UIColor.clear
        view.layer.borderWidth = 2
        view.layer.borderColor = contrast.cgColor
    }
    
    
    func localizedString(key : String) -> String {
        return Bundle.main.localizedString(forKey: key, value: "", table: "Strings")
    }
}

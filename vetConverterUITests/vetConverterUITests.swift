//
//  vetConverterUITests.swift
//  vetConverterUITests
//
//  Created by Davide Ricci on 05/08/17.
//  Copyright © 2017 Davide Ricci. All rights reserved.
//

import XCTest

@objcMembers
class vetConverterUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false

        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        snapshot("1")
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element
        let element = element2.children(matching: .other).element(boundBy: 0)
        element.swipeLeft()
        
        let textField = element2.children(matching: .textField).matching(identifier: "-").element(boundBy: 0)
        textField.tap()
        textField.typeText("12")
        
        let textField2 = element2.children(matching: .textField).matching(identifier: "-").element(boundBy: 2)
        textField2.tap()
        textField2.typeText("45")
        
        let textField3 = element2.children(matching: .textField).matching(identifier: "-").element(boundBy: 3)
        textField3.tap()
        textField3.typeText("3.5")
        
        let calculateNeededProductButton = app.buttons["Calculate needed product"]
        snapshot("2")
        calculateNeededProductButton.tap()
        element.swipeRight()
        textField.tap()
        
        var deleteKey = app.keys["Delete"]
        if (app.keys["Elimina"].exists) {
            deleteKey = app.keys["Elimina"]
        } else {
            deleteKey = app.keys["Delete"]
        }
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        textField.typeText("25")
        
        let textField4 = element2.children(matching: .textField).matching(identifier: "-").element(boundBy: 1)
        textField4.tap()
        textField4.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        textField4.typeText("200")
        textField2.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        textField2.typeText("47.5")
        element2.children(matching: .button).matching(identifier: "|||").element(boundBy: 2).tap()
        snapshot("3")
        textField3.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        textField3.typeText("9")
        calculateNeededProductButton.tap()
        snapshot("4")
    }
    
    func screenshot() {
    }
    
}
